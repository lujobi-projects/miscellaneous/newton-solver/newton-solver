use anyhow::bail;

use crate::{
    commons::*, expression::ExpressionTrait, expression_macros::binary::sub, BoxedExpression,
};

#[derive(Debug)]
pub struct Assignment {
    name: String,
    value: BoxedExpression,
}

impl Assignment {
    pub fn new(name: &str, value: BoxedExpression) -> Self {
        Self {
            name: name.to_string(),
            value,
        }
    }
}

impl std::fmt::Display for Assignment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} := {}", self.name, self.value)
    }
}

#[derive(Debug)]
pub struct Equation {
    left: BoxedExpression,
    right: BoxedExpression,
}

const MAX_ITERATIONS: usize = 100;

impl Equation {
    pub fn new(left: BoxedExpression, right: BoxedExpression) -> Self {
        Self { left, right }
    }
    pub fn numerically_solve(&self, name: VariableIDArg, start: Value) -> anyhow::Result<Value> {
        let solv_exp = sub!(self.left.clone(), self.right.clone()).simplify();
        if !solv_exp.only_depends_on(name) {
            bail!("Equation does not only depend on {}", name);
        }

        let df = solv_exp.derive(name).simplify();
        let mut x = start;

        for _ in 0..MAX_ITERATIONS {
            let f_x_val = solv_exp.substitute_var(name, no!(x)).eval();
            let df_dx_val = df.substitute_var(name, no!(x)).eval();
            let delta = f_x_val / df_dx_val;

            if delta.abs() < TOLERANCE {
                return Ok(x);
            }

            x -= delta;
        }

        bail!("Could not find solution in {} iterations", MAX_ITERATIONS);
    }
}

impl std::fmt::Display for Equation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} = {}", self.left, self.right)
    }
}
