#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use crate::{equation::Equation, expression::ExpressionTrait};

mod commons;
mod expression;
#[macro_use]
mod expression_macros;
mod equation;

use expression::BoxedExpression;
use expression_macros::*;

fn main() {
    let exp = mul!(add!(no!(1.0), no!(2.0)), sin!(var!(x)));
    println!("{}", exp);
    println!("{}", exp.simplify());
    let x = "x".to_string();
    println!("{}", exp.substitute_var(&x, no!(2.0)).simplify());
    println!(
        "{}",
        exp.substitute_var(&x, mul!(var!(a), var!("b"))).simplify()
    );
    println!("{}", exp.derive(&x));
    println!("{}", exp.derive(&x).simplify());

    let parser = parser::AssignmentParser::new();
    let parsed = parser.parse("x := 1").unwrap();
    println!("{:?}", parsed);
    let parsed = parser.parse("x := a").unwrap();
    println!("{:?}", parsed);

    println!("");

    let solv_eq = Equation::new(
        no!(0.0),
        sub!(
            div!(no!(5.0), sin!(sub!(div!(var!(x), no!(20.0)), no!(6.0)))),
            no!(50.0)
        ),
    );
    println!("{}", solv_eq);
    match solv_eq.numerically_solve("x", -3.5) {
        Ok(x) => println!("x = {}", x),
        Err(e) => println!("Error: {}", e),
    }

    let solv_eq = Equation::new(
        sub!(
            div!(no!(5.0), sin!(sub!(div!(var!(x), no!(20.0)), no!(6.0)))),
            no!(50.0)
        ),
        no!(0.0),
    );
    println!("{}", solv_eq);
    match solv_eq.numerically_solve("x", -3.5) {
        Ok(x) => println!("x = {}", x),
        Err(e) => println!("Error: {}", e),
    }
}
