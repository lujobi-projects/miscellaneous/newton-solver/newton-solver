pub type VariableID = String;
pub type VariableIDArg<'a> = &'a str;
pub type Value = f32;
pub type Precedence = u8;

pub static TOLERANCE: Value = 1e-3;
