macro_rules! un_exp {
    ($e:ident, $exp:expr) => {
        Box::new(crate::expression::expression_wrapper::Expression::Unary(
            crate::expression::unary::UnaryExpression::new(
                crate::expression::unary::UnaryOperand::$e,
                $exp,
            ),
        ))
    };
    ($e:expr, $exp:expr) => {
        Box::new(crate::expression::expression_wrapper::Expression::Unary(
            crate::expression::unary::UnaryExpression::new($e, $exp),
        ))
    };
}
pub(crate) use un_exp;

macro_rules! sin {
    ($exp:expr) => {
        crate::expression_macros::unary::un_exp!(Sin, $exp)
    };
}
pub(crate) use sin;
macro_rules! cos {
    ($exp:expr) => {
        crate::expression_macros::unary::un_exp!(Cos, $exp)
    };
}
pub(crate) use cos;
macro_rules! neg {
    ($exp:expr) => {
        crate::expression_macros::unary::un_exp!(Neg, $exp)
    };
}
pub(crate) use neg;
macro_rules! ln {
    ($exp:expr) => {
        crate::expression_macros::unary::un_exp!(Ln, $exp)
    };
}
pub(crate) use ln;
