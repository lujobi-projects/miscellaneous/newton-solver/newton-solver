macro_rules! bin_exp {
    ($e:ident, $lhs:expr, $rhs:expr) => {
        Box::new(crate::expression::expression_wrapper::Expression::Binary(
            crate::expression::binary::BinaryExpression::new(
                crate::expression::binary::BinaryOperand::$e,
                $lhs,
                $rhs,
            ),
        ))
    };
    ($e:expr, $lhs:expr, $rhs:expr) => {
        Box::new(crate::expression::expression_wrapper::Expression::Binary(
            crate::expression::binary::BinaryExpression::new($e, $lhs, $rhs),
        ))
    };
}
pub(crate) use bin_exp;

macro_rules! add {
    ($lhs:expr, $rhs:expr) => {
        crate::expression_macros::binary::bin_exp!(Add, $lhs, $rhs)
    };
}
pub(crate) use add;

macro_rules! sub {
    ($lhs:expr, $rhs:expr) => {
        crate::expression_macros::binary::bin_exp!(Sub, $lhs, $rhs)
    };
}
pub(crate) use sub;

macro_rules! mul {
    ($lhs:expr, $rhs:expr) => {
        crate::expression_macros::binary::bin_exp!(Mul, $lhs, $rhs)
    };
}
pub(crate) use mul;

macro_rules! div {
    ($lhs:expr, $rhs:expr) => {
        crate::expression_macros::binary::bin_exp!(Div, $lhs, $rhs)
    };
}
pub(crate) use div;

macro_rules! pow {
    ($lhs:expr, $rhs:expr) => {
        crate::expression_macros::binary::bin_exp!(Pow, $lhs, $rhs)
    };
}
pub(crate) use pow;
