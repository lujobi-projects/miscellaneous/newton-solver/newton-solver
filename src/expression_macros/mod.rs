pub mod binary;
pub mod unary;

pub(crate) use binary::*;
pub(crate) use unary::*;

macro_rules! no {
    ($no:expr) => {
        Box::new(crate::expression::expression_wrapper::Expression::Number(
            crate::expression::number::Number::new($no),
        ))
    };
}
pub(crate) use no;

macro_rules! var {
    ($name:ident) => {
        Box::new(crate::expression::expression_wrapper::Expression::Variable(
            crate::expression::variable::Variable::new(stringify!($name)),
        ))
    };
    ($name:expr) => {
        Box::new(crate::expression::expression_wrapper::Expression::Variable(
            crate::expression::variable::Variable::new($name),
        ))
    };
}
pub(crate) use var;

macro_rules! one {
    () => {
        Box::new(crate::expression::ONE.clone())
    };
}
pub(crate) use one;

macro_rules! zero {
    () => {
        Box::new(crate::expression::ZERO.clone())
    };
}
pub(crate) use zero;
