use crate::{
    commons::*,
    expression_macros::{cos, div, mul, neg, no, sin, un_exp},
};

use super::{BoxedExpression, ExpressionTrait};

#[derive(Clone, Debug, PartialEq)]
pub enum UnaryOperand {
    Neg,
    Sin,
    Cos,
    Ln,
}

#[derive(Debug, Clone, PartialEq)]
pub struct UnaryExpression {
    op: UnaryOperand,
    expr: BoxedExpression,
}

impl std::fmt::Display for UnaryExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.op {
            UnaryOperand::Neg => {
                if self.expr.precedence() > self.precedence() {
                    write!(f, "-({})", self.expr)
                } else {
                    write!(f, "-{}", self.expr)
                }
            }
            UnaryOperand::Sin => write!(f, "sin({})", self.expr),
            UnaryOperand::Cos => write!(f, "cos({})", self.expr),
            UnaryOperand::Ln => write!(f, "ln({})", self.expr),
        }
    }
}

impl ExpressionTrait for UnaryExpression {
    fn eval(&self) -> Value {
        match self.op {
            UnaryOperand::Neg => -self.expr.eval(),
            UnaryOperand::Sin => self.expr.eval().sin(),
            UnaryOperand::Cos => self.expr.eval().cos(),
            UnaryOperand::Ln => self.expr.eval().ln(),
        }
    }

    fn substitute_var(&self, name: VariableIDArg, value: BoxedExpression) -> BoxedExpression {
        un_exp!(self.op.clone(), self.expr.substitute_var(name, value))
    }

    fn depends_on(&self, name: VariableIDArg) -> bool {
        self.expr.depends_on(name)
    }

    fn is_numerically_simplifyable(&self) -> bool {
        self.expr.is_numerically_simplifyable()
    }

    fn simplify(&self) -> BoxedExpression {
        un_exp!(self.op.clone(), self.expr.simplify())
    }

    fn derive(&self, name: VariableIDArg) -> BoxedExpression {
        match self.op {
            UnaryOperand::Neg => neg!(self.expr.derive(name)),
            UnaryOperand::Sin => mul!(cos!(self.expr.clone()), self.expr.derive(name)),
            UnaryOperand::Cos => neg!(mul!(sin!(self.expr.clone()), self.expr.derive(name))),
            UnaryOperand::Ln => {
                mul!(div!(no!(1.0), self.expr.clone()), self.expr.derive(name))
            }
        }
    }

    fn only_depends_on(&self, name: VariableIDArg) -> bool {
        self.expr.only_depends_on(name)
    }
}

impl UnaryExpression {
    pub fn new(op: UnaryOperand, expr: BoxedExpression) -> Self {
        Self { op, expr }
    }
}
