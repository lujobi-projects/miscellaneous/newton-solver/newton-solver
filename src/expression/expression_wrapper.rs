use crate::expression::{
    binary::BinaryExpression, number::Number, unary::UnaryExpression, variable::Variable,
};

use super::{BoxedExpression, ExpressionTrait, Precedence, Value, VariableIDArg};

/**
 * This is merely a wrapper for the different types of expressions to prevent dynamic dispatch.
 * Also this simplifies the implementation of the `PartialEq` trait.
 */

#[derive(Clone, Debug, PartialEq)]
pub enum Expression {
    Number(Number),
    Variable(Variable),
    Binary(BinaryExpression),
    Unary(UnaryExpression),
}

impl std::fmt::Display for Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Expression::Number(n) => n.fmt(f),
            Expression::Variable(v) => v.fmt(f),
            Expression::Binary(b) => b.fmt(f),
            Expression::Unary(u) => u.fmt(f),
        }
    }
}

impl ExpressionTrait for Expression {
    fn eval(&self) -> Value {
        match self {
            Expression::Number(n) => n.eval(),
            Expression::Variable(v) => v.eval(),
            Expression::Binary(b) => b.eval(),
            Expression::Unary(u) => u.eval(),
        }
    }

    fn substitute_var(&self, name: VariableIDArg, value: BoxedExpression) -> BoxedExpression {
        match self {
            Expression::Number(n) => n.substitute_var(name, value),
            Expression::Variable(v) => v.substitute_var(name, value),
            Expression::Binary(b) => b.substitute_var(name, value),
            Expression::Unary(u) => u.substitute_var(name, value),
        }
    }

    fn depends_on(&self, name: VariableIDArg) -> bool {
        match self {
            Expression::Number(n) => n.depends_on(name),
            Expression::Variable(v) => v.depends_on(name),
            Expression::Binary(b) => b.depends_on(name),
            Expression::Unary(u) => u.depends_on(name),
        }
    }

    fn is_numerically_simplifyable(&self) -> bool {
        match self {
            Expression::Number(n) => n.is_numerically_simplifyable(),
            Expression::Variable(v) => v.is_numerically_simplifyable(),
            Expression::Binary(b) => b.is_numerically_simplifyable(),
            Expression::Unary(u) => u.is_numerically_simplifyable(),
        }
    }

    fn simplify(&self) -> BoxedExpression {
        match self {
            Expression::Number(n) => n.simplify(),
            Expression::Variable(v) => v.simplify(),
            Expression::Binary(b) => b.simplify(),
            Expression::Unary(u) => u.simplify(),
        }
    }

    fn derive(&self, name: VariableIDArg) -> BoxedExpression {
        match self {
            Expression::Number(n) => n.derive(name),
            Expression::Variable(v) => v.derive(name),
            Expression::Binary(b) => b.derive(name),
            Expression::Unary(u) => u.derive(name),
        }
    }

    fn precedence(&self) -> Precedence {
        match self {
            Expression::Number(n) => n.precedence(),
            Expression::Variable(v) => v.precedence(),
            Expression::Binary(b) => b.precedence(),
            Expression::Unary(u) => u.precedence(),
        }
    }

    fn only_depends_on(&self, name: VariableIDArg) -> bool {
        match self {
            Expression::Number(n) => n.only_depends_on(name),
            Expression::Variable(v) => v.only_depends_on(name),
            Expression::Binary(b) => b.only_depends_on(name),
            Expression::Unary(u) => u.only_depends_on(name),
        }
    }
}
