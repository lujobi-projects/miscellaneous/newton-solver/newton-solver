use crate::{
    commons::*,
    expression_macros::{no, zero},
};

use super::{BoxedExpression, ExpressionTrait};

#[derive(Clone, Debug, PartialEq)]
pub struct Number {
    value: Value,
}

impl std::fmt::Display for Number {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.value.fmt(f)
    }
}

impl ExpressionTrait for Number {
    fn eval(&self) -> Value {
        self.value
    }

    fn substitute_var(&self, _name: VariableIDArg, _value: BoxedExpression) -> BoxedExpression {
        no!(self.value)
    }

    fn depends_on(&self, _name: VariableIDArg) -> bool {
        false
    }

    fn is_numerically_simplifyable(&self) -> bool {
        true
    }

    fn simplify(&self) -> BoxedExpression {
        no!(self.value)
    }

    fn derive(&self, _name: VariableIDArg) -> BoxedExpression {
        zero!()
    }

    fn only_depends_on(&self, _name: VariableIDArg) -> bool {
        true
    }
}

impl Number {
    pub const fn new(value: Value) -> Self {
        Self { value }
    }
}
