use crate::expression_macros::{add, bin_exp, div, mul, no, one, sub, unary::neg, zero};

use super::{BoxedExpression, ExpressionTrait, Precedence, Value, VariableIDArg, ONE, ZERO};

#[derive(Clone, Debug, PartialEq)]
pub enum BinaryOperand {
    Add,
    Sub,
    Mul,
    Div,
    Pow,
}

#[derive(Clone, Debug, PartialEq)]
pub struct BinaryExpression {
    op: BinaryOperand,
    lhs: BoxedExpression,
    rhs: BoxedExpression,
}

impl std::fmt::Display for BinaryExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let lhs = self.lhs.to_string();
        let rhs = self.rhs.to_string();

        let op = match self.op {
            BinaryOperand::Add => "+",
            BinaryOperand::Sub => "-",
            BinaryOperand::Mul => "*",
            BinaryOperand::Div => "/",
            BinaryOperand::Pow => "^",
        };

        if self.lhs.precedence() > self.precedence() {
            write!(f, "({}) {} {}", lhs, op, rhs)
        } else if self.rhs.precedence() > self.precedence() {
            write!(f, "{} {} ({})", lhs, op, rhs)
        } else {
            write!(f, "{} {} {}", lhs, op, rhs)
        }
    }
}

impl ExpressionTrait for BinaryExpression {
    fn eval(&self) -> Value {
        let lhs = self.lhs.eval();
        let rhs = self.rhs.eval();

        match self.op {
            BinaryOperand::Add => lhs + rhs,
            BinaryOperand::Sub => lhs - rhs,
            BinaryOperand::Mul => lhs * rhs,
            BinaryOperand::Div => lhs / rhs,
            BinaryOperand::Pow => lhs.powf(rhs),
        }
    }

    fn substitute_var(&self, name: VariableIDArg, value: BoxedExpression) -> BoxedExpression {
        bin_exp!(
            self.op.clone(),
            self.lhs.substitute_var(name, value.clone()),
            self.rhs.substitute_var(name, value)
        )
    }

    fn depends_on(&self, name: VariableIDArg) -> bool {
        self.lhs.depends_on(name) || self.rhs.depends_on(name)
    }

    fn is_numerically_simplifyable(&self) -> bool {
        self.lhs.is_numerically_simplifyable() && self.rhs.is_numerically_simplifyable()
    }

    fn precedence(&self) -> Precedence {
        match self.op {
            BinaryOperand::Add => super::PRECEDENCE_ADD,
            BinaryOperand::Sub => super::PRECEDENCE_SUB,
            BinaryOperand::Mul => super::PRECEDENCE_MUL,
            BinaryOperand::Div => super::PRECEDENCE_DIV,
            BinaryOperand::Pow => super::PRECEDENCE_POW,
        }
    }

    fn simplify(&self) -> BoxedExpression {
        let (new_left, new_right) = match (
            self.lhs.is_numerically_simplifyable(),
            self.rhs.is_numerically_simplifyable(),
        ) {
            (true, true) => {
                return no!(self.eval());
            }
            (true, false) => (self.lhs.simplify(), self.rhs.clone()),
            (false, true) => (self.lhs.clone(), self.rhs.simplify()),
            (false, false) => (self.lhs.simplify(), self.rhs.simplify()),
        };

        match (self.op.clone(), new_left, new_right) {
            // add and subtract zero
            (BinaryOperand::Add, lhs, rhs) if *rhs == ZERO => lhs,
            (BinaryOperand::Add, lhs, rhs) if *lhs == ZERO => rhs,
            (BinaryOperand::Sub, lhs, rhs) if *rhs == ZERO => lhs,
            (BinaryOperand::Sub, lhs, rhs) if *lhs == ZERO => neg!(rhs),
            // multiply by zero or one
            (BinaryOperand::Mul, _, rhs) if *rhs == ZERO => zero!(),
            (BinaryOperand::Mul, lhs, _) if *lhs == ZERO => zero!(),
            (BinaryOperand::Mul, lhs, rhs) if *rhs == ONE => lhs,
            (BinaryOperand::Mul, lhs, rhs) if *lhs == ONE => rhs,
            // divide by one or zero in the numerator
            (BinaryOperand::Div, lhs, _) if *lhs == ZERO => zero!(),
            (BinaryOperand::Div, lhs, rhs) if *rhs == ONE => lhs,
            // raise to the power of zero or one
            (BinaryOperand::Pow, _, rhs) if *rhs == ZERO => one!(),
            (BinaryOperand::Pow, lhs, _) if *lhs == ZERO => zero!(),
            (BinaryOperand::Pow, lhs, rhs) if *rhs == ONE => lhs,
            (BinaryOperand::Pow, lhs, _) if *lhs == ONE => no!(1.0),
            (op, lhs, rhs) => bin_exp!(op.clone(), lhs, rhs),
        }
    }

    fn derive(&self, name: VariableIDArg) -> BoxedExpression {
        match self.op {
            BinaryOperand::Add => add!(self.lhs.derive(name), self.rhs.derive(name)),
            BinaryOperand::Sub => sub!(self.lhs.derive(name), self.rhs.derive(name)),
            BinaryOperand::Mul => add!(
                mul!(self.lhs.derive(name), self.rhs.clone()),
                mul!(self.lhs.clone(), self.rhs.derive(name))
            ),
            BinaryOperand::Div => {
                let lhs = mul!(self.lhs.derive(name), self.rhs.clone());
                let rhs = mul!(self.lhs.clone(), self.rhs.derive(name));
                div!(sub!(lhs, rhs), mul!(self.rhs.clone(), self.rhs.clone()))
            }
            BinaryOperand::Pow => {
                todo!();
            }
        }
    }

    fn only_depends_on(&self, name: VariableIDArg) -> bool {
        self.lhs.only_depends_on(name) && self.rhs.only_depends_on(name)
    }
}

impl BinaryExpression {
    pub fn new(op: BinaryOperand, lhs: BoxedExpression, rhs: BoxedExpression) -> Self {
        Self { op, lhs, rhs }
    }
}
