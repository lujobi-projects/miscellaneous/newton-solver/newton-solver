use crate::{
    commons::*,
    expression_macros::{one, var, zero},
};

use super::{BoxedExpression, ExpressionTrait};

#[derive(Debug, Clone, PartialEq)]
pub struct Variable {
    name: VariableID,
}

impl std::fmt::Display for Variable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.name.fmt(f)
    }
}

impl ExpressionTrait for Variable {
    fn eval(&self) -> Value {
        todo!()
    }

    fn substitute_var(&self, name: VariableIDArg, value: BoxedExpression) -> BoxedExpression {
        if *name == self.name {
            value.clone()
        } else {
            var!(self.name.clone().as_str())
        }
    }

    fn depends_on(&self, name: VariableIDArg) -> bool {
        *name == self.name
    }

    fn is_numerically_simplifyable(&self) -> bool {
        false
    }

    fn simplify(&self) -> BoxedExpression {
        var!(self.name.clone().as_str())
    }

    fn derive(&self, name: VariableIDArg) -> BoxedExpression {
        if self.name == *name {
            one!()
        } else {
            zero!()
        }
    }

    fn only_depends_on(&self, name: VariableIDArg) -> bool {
        self.name == *name
    }
}

impl Variable {
    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
        }
    }
}
