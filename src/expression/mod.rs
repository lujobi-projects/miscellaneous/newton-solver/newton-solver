use crate::commons::*;

use self::{expression_wrapper::Expression, number::Number};

pub mod binary;
pub mod expression_wrapper;
pub mod number;
pub mod unary;
pub mod variable;

pub static PRECEDENCE_ADD: Precedence = 1;
pub static PRECEDENCE_SUB: Precedence = 1;
pub static PRECEDENCE_MUL: Precedence = 2;
pub static PRECEDENCE_DIV: Precedence = 2;
pub static PRECEDENCE_POW: Precedence = 3;

pub type BoxedExpression = Box<Expression>;

pub trait ExpressionTrait: std::fmt::Debug + std::fmt::Display + PartialEq {
    fn eval(&self) -> Value;
    fn substitute_var(&self, name: VariableIDArg, value: BoxedExpression) -> BoxedExpression;
    fn depends_on(&self, name: VariableIDArg) -> bool;
    fn only_depends_on(&self, name: VariableIDArg) -> bool;
    fn is_numerically_simplifyable(&self) -> bool;
    fn simplify(&self) -> BoxedExpression;
    fn derive(&self, name: VariableIDArg) -> BoxedExpression;
    fn precedence(&self) -> Precedence {
        0
    }
}

pub static ZERO: Expression = Expression::Number(Number::new(0.0));
pub static ONE: Expression = Expression::Number(Number::new(1.0));
